#include "stdafx.h"
#include "Mail.h"
#include <string>

Mail::Mail() :from(), to(), name(string("a")), mess() {};

Mail::Mail(int *fro, int *t, const string &c, Message &m) : name(c), mess(m)
{
	if (c.size() > 20) throw "INCORRECT NAME";
	for (int i = 0; i < 4; i++) {
		if ((fro[i] < 0) || (fro[i] > 255) || (t[i] < 0) || (t[i] > 255))
			throw "INCORRECT ADRESS";
		from[i] = fro[i];
		to[i] = t[i];
	}
}


Mail::~Mail() {}

Mail::Mail(const Mail &a) {

	name = a.name;
	for (int i = 0; i < 4; i++) {
		from[i] = a.from[i];
		to[i] = a.to[i];
	}

	mess = a.mess;
}

Mail::Mail(Mail &&a) {
	int from[4];
	int to[4];


	string name;
	Message mess;

	for (int i = 0; i < 4; i++) {
		from[i] = a.from[i];
		to[i] = a.to[i];
	}

	name = a.name;
	mess = a.mess;

	for (int i = 0; i < 4; i++) {
		a.from[i] = 0;
		a.to[i] = 0;
	}

	a.name = "";
}


int *Mail::get_from()
{
	return from;
}


int * Mail::get_to()
{
	return to;
}


string Mail::get_name() const
{
	return name;
}

Message Mail::get_message() const
{
	return mess;
}

void Mail::set_from(int *t) {
	for (int i = 0; i < 4; i++) {
		if ((t[i] < 0) || (t[i] > 255))
			throw "INCORRECT ADRESS";
		from[i] = t[i];
	}
}

void Mail::set_to(int *t) {
	for (int i = 0; i < 4; i++) {
		if ((t[i] < 0) || (t[i] > 255))
			throw "INCORRECT ADRESS";
		to[i] = t[i];
	}

}

void Mail::set_name(const string a) {
	name.assign(a);
}

void Mail::set_message(Message &m) {
	mess = m;
}

Mail *Mail::clone()
{
	return new Mail(*this);
}

ostream & Mail::print(ostream &c) const
{
	c << "##########" << endl << endl;
	c << "USER NAME: " << name << endl;
	c << "MESSAGE TEXT: " << mess.get_text() << endl;
	c << "FROM: " << from[0] << "." << from[1] << "." << from[2] << "." << from[3] << endl;
	c << "TO: " << to[0] << "." << to[1] << "." << to[2] << "." << to[3] << endl;
	c << "PACKAGE TYPE: MAIL" << endl;

	return c;
}







