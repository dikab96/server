#include "stdafx.h"
#include "Link.h"


Link::Link(): typeProt(0), name(string("www.MEPHI")){}

Link::Link(int x, const string &a) : typeProt(x), name(a) { if (x != 0 && x != 1) throw "INCORRECT TYPE OF PROT"; }

Link::~Link(){}

Link::Link(const Link &l){
name = l.name;
typeProt= l.typeProt;
}

string Link::get_name() const
{
	return name;
}


int Link::get_typeProt() const
{
	return typeProt;
}

void Link::set_typeProt(int x)
{
	if (x != 0 && x != 1) throw "INCORRECT TYPE OF PROT";
	typeProt = x;
}

void Link::set_name(const string a)
{
	name.assign(a);
}
