#include "stdafx.h"
#include "Hypertext.h"


Hypertext::Hypertext(): link(0) {}

Hypertext::Hypertext(vector <Link> mas, File &f) :link(mas), File(f) {}


Hypertext::~Hypertext(){}

Hypertext::Hypertext(const Hypertext &a) : File(a) {

	link = a.link;
}



vector<Link> Hypertext::get_link()
{
	return link;
}


void Hypertext::set_link(vector <Link> mas) {
	link = mas;
}

Hypertext *Hypertext::convert_to_hypertext(File *f)
{
	Hypertext *H = dynamic_cast<Hypertext *>(f);
	return H;
}

Mail *Hypertext::clone() {
	return new Hypertext(*this);
}

ostream & Hypertext::print(ostream &c) const
{
	Mail::print(c);
	if (typeKod == 0)
		c << "TYPE KOD: ASCII" << endl;
	else
		c << "TYPE KOD: BIN" << endl;
	if (typeInf == 0)
		c << "TYPE INFO: CONTROL" << endl;
	else
		c << "TYPE INFO: DATA" << endl;

	c << "AMOUNT OF LINKS" << link.size() << endl;
	c << "PACKAGE TYPE: HYPERTEXT" << endl;

	return c;
}
