#pragma once
#include<iostream>
#include "stdafx.h"


template <class Item>
class Queue {
private:

	Item *q;
	int head, tail;
public:

	Queue();
	//! default initialization constructor

	void put(Item);
	void delet();

	Item get_front();
	int get_head();
	int get_tail();

};


template<class Item> inline Queue<Item>::Queue():head(0), tail(0)
{
	q = new Item[10];
}

template<class Item> inline void Queue<Item>::put(Item item){
	q[tail] = item;
	tail++;
}

template<class Item> inline void Queue<Item>::delet()
{
	head++;
}

template<class Item> inline Item Queue<Item>::get_front()
{
	return q[head];
}


template<class Item> inline int Queue<Item>::get_head()
{
	return head;
}

template<class Item> inline int Queue<Item>::get_tail()
{
	return tail;
}





