#pragma once
#include <string>
#include <iostream>
#include "Mail.h"
#include "Message.h"

using namespace std;


class File:public Mail {
protected:
	int typeKod;
	int typeInf;

public:
	File();
	//! default initialization constructor
	File(int, int, Mail &);
	//! initialization constructor
	//! takes (typeKod(int),typeInf( int), mail( Mail))
	~File();
	//! default destructor
	File(const File &);

	int get_typeKod()const ;
	//! return typeKod
	int get_typeInf()const ;
	//! return typeInf

	void set_typeKod(int );
	//! edits file's typeKod
	//! takes int
	void set_typeInf(int );
	//! edits file's typeInf
	//! takes int

	File *convert_to_file(Mail *);
	//! convert from Mail to File
	//! takes Mail
	//! return File
	Mail *clone();
	//! make a copy of File
	ostream &print(ostream &) const;
	//! output function 
	//! displays full information about File
	//! Takes a reference to the output stream
	//! returns a reference to the output stream
};

