
#pragma once
#include <string>
#include <iostream>
#include "File.h"
#include "Message.h"
#include "Link.h"
#include <vector>

using namespace std;

class Hypertext :public File
{
protected:

	vector<Link > link;
public:

	Hypertext();
	//! default initialization constructor
	Hypertext(vector <Link>, File &);
	//! initialization constructor
	//! takes (vector of links(vector <Link>,),file( File))
	~Hypertext();
	//! default destructor
	Hypertext(const Hypertext &);

	vector <Link> get_link();
	//!return vector of link

	void set_link(vector <Link>);
	//! edits server's adress
	//! takes vector of link

	Hypertext *convert_to_hypertext(File *);
	//! convert from File to Hypertext
	//! takes File
	//! return Hypertext
	Mail *clone();
	//! makes a copy of Hypertext
	ostream &print(ostream &) const;
};

