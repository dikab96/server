#pragma once
#include<iostream>



template<class T>
class my_vectorIt
{
private:
	T* It;
public:
	my_vectorIt();
	my_vectorIt(T&);

	int operator==(const my_vectorIt<T>&)const;
	int operator!=(const my_vectorIt<T>&)const;
	T operator*();
	my_vectorIt<T>& operator++();
	my_vectorIt<T> operator++(int);

	my_vectorIt<T>& operator--();
	my_vectorIt<T> operator--(int);


	//~my_vectorIt();
	//friend my_vector<T>;
};


template<class T>
my_vectorIt<T>::my_vectorIt() :
	It(nullptr)
{}

template<class T>
my_vectorIt<T>::my_vectorIt(T& t) :
	It(&t)
{}

template<class T>
T my_vectorIt<T>::operator*()
{
	return *It;
}

template<class T>
int my_vectorIt<T>::operator==(const my_vectorIt<T>&I)const
{
	return It == I.It;
}

template<class T>
inline int my_vectorIt<T>::operator!=(const my_vectorIt<T>&  I)const
{
	return It != I.It;
}

template<class T>
my_vectorIt<T>&my_vectorIt<T>:: operator++()
{
	It++;
	return	*this;
}

template<class T>
inline my_vectorIt<T> my_vectorIt<T>::operator++(int)
{
	return my_vectorIt<T>();
}

template<class T>
my_vectorIt<T>&my_vectorIt<T>:: operator--()
{
	--It;
	return	*this;
}


template<class T>
my_vectorIt<T> my_vectorIt<T>:: operator--(int)
{
	my_vectorIt res(*this);
	--It;
	return res;
}

template<class T>
class my_vector
{
private:
	static const int maxSize = 1000;
	int Size;
	T* arr;
public:

	my_vector();

	int size()const;
	my_vector<T>(const my_vector<T>&);
	my_vector<T>(const my_vector<T>&&);
	void push_back(const T&);
	void del(int);

	my_vector<T>& operator=(const my_vector<T>&);
	my_vector<T>& operator=(const my_vector<T>&&);
	int operator==(const my_vector<T>&)const;
	T& operator[](int)const;

	const my_vectorIt<T>& begin();
	my_vectorIt<T>& end();

	void swap(my_vector<T>&);
	~my_vector();
	//friend my_vectorIt<T>;
};


template<class T>
const my_vectorIt<T>& my_vector<T>::begin()
{
	my_vectorIt<T> I(arr[0]);
	return I;
}

template<class T>
my_vectorIt<T>& my_vector<T>::end()
{
	return my_vectorIt<T>(arr[Size]);
}

template<class T>
my_vector<T>::my_vector() :
	Size(0), arr(nullptr)
{
}

template<class T>
my_vector<T>::~my_vector()
{
	if (arr)
		delete[]arr;
}

template<class T>
my_vector<T>::my_vector(const my_vector<T>&a)
{
	if (!a.Size)
	{
		if (!arr)
			delete[]arr;
		arr = nullptr;
		Size = 0;
		return;
	}

	if (!arr)
		delete[]arr;
	arr = new T[a.Size];


	for (int i = 0; i < a.Size; ++i)
		arr[i] = a.arr[i];

	Size = a.Size;
}

template<class T>
my_vector<T>::my_vector(const my_vector<T>&&a)
{
	if (this == &a)
		return;
	T* ptrarr = arr;
	arr = a.arr;
	Size = a.Size;
	a.arr = ptrarr;
}

template<class T>
int my_vector<T>::size() const
{
	return Size;
}

template<class T>
void my_vector<T>::push_back(const T &a)
{
	if (maxSize < Size + 1)
		throw "max size";

	T*arr1 = new T[++Size];
	for (int i = 0; i < Size - 1; ++i)
		arr1[i] = arr[i];
	arr1[Size - 1] = a;
	if (!arr)
		delete[]arr;
	arr = arr1;
	return;
}

template<class T>
void my_vector<T>::del(int n)
{
	if (n<0 || n>Size)
		throw "incorrect chto to";


	if (n >= Size)
		throw "incorrect index";
	T*arr1 = new T[Size - 1];

	for (int i = 0; i < n; ++i)
		arr1[i] = arr[i];
	for (int i = n + 1; i < Size; ++i)
		arr1[i - 1] = arr[i];
	--Size;

	delete[]arr;
	arr = arr1;

	return;
}

template<class T>
my_vector<T>& my_vector<T>::operator=(const my_vector<T>& a)
{
	if (!a.size())
	{
		if (!arr)
			delete[]arr;
		Size = 0;
		return *this;
	}

	if (!arr)
		delete[]arr;

	arr = new T[a.size()];


	for (int i = 0; i < a.size(); ++i)
		arr[i] = a.arr[i];

	Size = a.size();
	return *this;
}

template<class T>
my_vector<T>& my_vector<T>::operator=(const my_vector<T>&& a)
{
	if (this == &a)
		return *this;
	T* ptrarr = arr;
	arr = a.arr;
	Size = a.Size;
	a.arr = ptrarr;
	return *this;
}

template<class T>
int my_vector<T>::operator==(const my_vector<T>& a)const
{
	if (a.size() != Size)
		return 0;

	for (int i = 0; i < Size; ++i)
		if (arr[i] != a.arr[i])
			return 0;
	return 1;
}

template<class T>
void swap(my_vector<T>& a, my_vector<T>& b)
{
	int tmp = a.Size;
	T*tmparr = a.arr;
	a.arr = b.arr;
	a.Size = b.Size;
	b.arr = tmparr;
	b.Size = tmp;
}

template<class T>
void my_vector<T>::swap(my_vector<T> &a)
{
	int tmp = a.Size;
	T*tmparr = a.arr;
	a.arr = arr;
	a.Size = Size;
	arr = tmparr;
	Size = tmp;
}

template<class T>
T& my_vector<T>::operator[](int i)const
{
	if (i<0 || i>maxSize)
		throw "incorrect index";

	return arr[i];
}
