#include "stdafx.h"
#include "Server.h"




Server::Server(): name("TEST SERVER"), adress(){}


Server::~Server(){}

Server::Server(const Server &s) {
	name = s.name;
	for (int i = 0; i < 4; i++) {
		adress[i] = s.adress[i];
	}
	Table = s.Table;
}


Queue <Mail *> Server::get_Table()const {
	return Table;
}

string Server::get_name()const {
	return name;
}

int * Server::get_adress() {
	return adress;
}

void Server::set_name(const string a) {
	name.assign(a);
}

void Server::set_adress(int *a) {
	for (int i = 0; i < 4; i++) {
		if ((a[i] < 0) || (a[i] > 255))
			throw "INCORRECT ADRESS";
		adress[i] = a[i];
	}
}

Server &Server::add_package(Mail *m)
{
	Mail *a = m->clone();
	Table.put(a);
	return *this;
}



Mail *Server::Find_package(int *t, int type)
{
	/*int i = Table.size();
	if (i == 0) {
		cout << "Empty table";
		return nullptr;
	}
	for (i; i > 0; i--) {
		Mail *m = Table.front();
		if ((m->get_to()[0] == t[0]) && (m->get_to()[1] == t[1]) && (m->get_to()[2] == t[2]) && (m->get_to()[3] == t[3])) {
			File *file = nullptr; file = file->convert_to_file(m);
			if ((file == nullptr) && (type == 1))
				return m;

			Hypertext *hyp = nullptr;
			hyp = hyp->convert_to_hypertext(file);
			if ((hyp != nullptr) && (type == 3))
				return hyp;

			if ((file != nullptr) && (type == 2) && (hyp == nullptr))
				return file;
		}
		Table.push(Table.front());
		Table.pop();
	}
	cout << "There is not this package" << endl;
	return nullptr;*/

	int i = Table.get_tail() - Table.get_head();
	if (i == 0) {
		cout << "Empty table";
		return nullptr;
	}
	for (i; i > 0; i--) {
		Mail *m = Table.get_front();
		if ((m->get_to()[0] == t[0]) && (m->get_to()[1] == t[1]) && (m->get_to()[2] == t[2]) && (m->get_to()[3] == t[3])) {
			File *file = nullptr; file = file->convert_to_file(m);
			if ((file == nullptr) && (type == 1))
				return m;

			Hypertext *hyp = nullptr;
			hyp = hyp->convert_to_hypertext(file);
			if ((hyp != nullptr) && (type == 3))
				return hyp;

			if ((file != nullptr) && (type == 2) && (hyp == nullptr))
				return file;
		}

		Table.put(Table.get_front());
		Table.delet();
	}
	cout << "There is not this package" << endl;
	return nullptr;
}


int Server::determine_type(Mail &m) {
	File *file = nullptr;
	Hypertext *hyp = nullptr;

	file = file->convert_to_file(&m);
	if (file == nullptr)
		return 1;
	if (file != nullptr) {
		hyp = hyp->convert_to_hypertext(file);
		if (hyp != nullptr)
			return 3;
	}
	return 2;
}

Server &Server::delete_package(int *t, int type) {
	/*int i = Table.size();
	if (i == 0) {
		cout << "Empty table";
		return *this;
	}
	for (i; i > 0; i--) {
		Mail *m = Table.front();
		if ((m->get_to()[0] == t[0]) && (m->get_to()[1] == t[1]) && (m->get_to()[2] == t[2]) && (m->get_to()[3] == t[3])) {
			File *file = nullptr; file = file->convert_to_file(m);
			if ((file == nullptr) && (type == 1)) {
				Table.pop();
				return *this;
			}
			if ((file != nullptr) && (type == 2)) {
				Table.pop();
				return *this;
			}
			Hypertext *hyp = nullptr;
			hyp = hyp->convert_to_hypertext(file);
			if ((hyp != nullptr) && (type == 3)) {
				Table.pop();
				return *this;
			}
		}
		Table.push(Table.front());
		Table.pop();
	}
	cout << "There is not this package" << endl << endl;
	return *this;*/
	int i = Table.get_tail() - Table.get_head();
	if (i == 0) {
		cout << "Empty table";
		return *this;
	}
	for (i; i > 0; i--) {
		Mail *m = Table.get_front();
		if ((m->get_to()[0] == t[0]) && (m->get_to()[1] == t[1]) && (m->get_to()[2] == t[2]) && (m->get_to()[3] == t[3])) {
			File *file = nullptr; file = file->convert_to_file(m);
			if ((file == nullptr) && (type == 1)) {
				Table.delet();
				return *this;
			}
			if ((file != nullptr) && (type == 2)) {
				Table.delet();
				return *this;
			}
			Hypertext *hyp = nullptr;
			hyp = hyp->convert_to_hypertext(file);
			if ((hyp != nullptr) && (type == 3)) {
				Table.delet();
				return *this;
			}
		}
		Table.put(Table.get_front());
		Table.delet();
	}
	cout << "There is not this package" << endl << endl;
	return *this;
}

std::ostream & operator<<(std::ostream &c, Server &server) {
	/*int i = server.Table.size();
	vector <Link> mas;
	c << "Server's name : " << server.name ;
	if (server.adress == nullptr) throw "There is not server's adress";
	c << " | Server's adress : " << server.adress[0] << "." << server.adress[1] << "." << server.adress[2] << "." << server.adress[3] << endl;
	for (i; i > 0; i--) {
		int flag = 0;
		Mail *m = server.Table.front();
		File *file = nullptr;
		Hypertext *hyp = nullptr;

		c << "##########" << endl << endl;
		c << "USER NAME: " << m->get_name() << endl;
		c << "MESSAGE TEXT: " << m->get_message().get_text() << endl;
		c << "FROM: " << m->get_from()[0] << "." << m->get_from()[1] << "." << m->get_from()[2] << "." << m->get_from()[3] << endl;
		c << "TO: " << m->get_to()[0] << "." << m->get_to()[1] << "." << m->get_to()[2] << "." << m->get_to()[3] << endl;


		if ((file = file->convert_to_file(m)) != nullptr) {
			if (file->get_typeKod() == 0)
				c << "TYPE KOD: ASCII" << endl;
			else
				c << "TYPE KOD: BIN" << endl;
			if (file->get_typeInf() == 0)
				c << "TYPE INFO: CONTROL" << endl;
			else
				c << "TYPE INFO: DATA" << endl;
		}
		else {
			c << "PACKAGE TYPE: MAIL" << endl;
			flag = 1;
		}

		if ((hyp = hyp->convert_to_hypertext(file)) != nullptr) {
			c << "AMOUNT OF LINKS: " << hyp->get_link().size() << endl;
			c << "LINKS: " << endl;
			mas = hyp->get_link();
			for (int i = 0;i < hyp->get_link().size(); i++) {
				c << "ITEM: " << i+1 << "TypeProt: " << mas[i].get_typeProt();
				c<< "NAME: " <<mas[i].get_name() << endl;
			}
			c << "PACKAGE TYPE: HYPERTEXT"  << endl;
		}

		else {
			if (flag == 0) {
				c << "PACKAGE TYPE: FILE" << endl;
			}
		}


		server.Table.push(server.Table.front());
		server.Table.pop();
	}
	return c;*/

	int i = server.Table.get_tail() - server.Table.get_head();
	vector <Link> mas;
	c << "Server's name : " << server.name;
	if (server.adress == nullptr) throw "There is not server's adress";
	c << " | Server's adress : " << server.adress[0] << "." << server.adress[1] << "." << server.adress[2] << "." << server.adress[3] << endl;
	for (i; i > 0; i--) {
		int flag = 0;
		Mail *m = server.Table.get_front();
		File *file = nullptr;
		Hypertext *hyp = nullptr;

		c << "##########" << endl << endl;
		c << "USER NAME: " << m->get_name() << endl;
		c << "MESSAGE TEXT: " << m->get_message().get_text() << endl;
		c << "FROM: " << m->get_from()[0] << "." << m->get_from()[1] << "." << m->get_from()[2] << "." << m->get_from()[3] << endl;
		c << "TO: " << m->get_to()[0] << "." << m->get_to()[1] << "." << m->get_to()[2] << "." << m->get_to()[3] << endl;


		if ((file = file->convert_to_file(m)) != nullptr) {
			if (file->get_typeKod() == 0)
				c << "TYPE KOD: ASCII" << endl;
			else
				c << "TYPE KOD: BIN" << endl;
			if (file->get_typeInf() == 0)
				c << "TYPE INFO: CONTROL" << endl;
			else
				c << "TYPE INFO: DATA" << endl;
		}
		else {
			c << "PACKAGE TYPE: MAIL" << endl;
			flag = 1;
		}

		if ((hyp = hyp->convert_to_hypertext(file)) != nullptr) {
			c << "AMOUNT OF LINKS: " << hyp->get_link().size() << endl;
			c << "LINKS: " << endl;
			mas = hyp->get_link();
			for (int i = 0;i < hyp->get_link().size(); i++) {
				c << "ITEM: " << i + 1 << "TypeProt: " << mas[i].get_typeProt();
				c << "NAME: " << mas[i].get_name() << endl;
			}
			c << "PACKAGE TYPE: HYPERTEXT" << endl;
		}

		else {
			if (flag == 0) {
				c << "PACKAGE TYPE: FILE" << endl;
			}
		}


		server.Table.put(server.Table.get_front());
		server.Table.delet();
	}
	return c;
}


int getNum(int &a) {
	cin >> a;
	if (!cin.good())
		return 0;
	if ((a > 255) || (a < 0)) throw "Incorrect adress";
	return 1;
}

string getStr(string &a) {
	cin >> a;
	if (!cin.good())
		return 0;
	return a;
}

