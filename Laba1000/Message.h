#pragma once
#include <string>
#include <iostream>
using namespace std;


class Message
{
private:
	string text;
public:
	Message();
	//! default initialization constructor
	Message(const string &);
	//! initialization constructor
	//! takes (text(string))
	~Message();
	//! default destructor
	Message(const Message &);
	int get_len()const;
	//! return len of message
	string get_text()const;
	//! return  message's text
	void set_text(const string );
	//! edits user's name
	//! takes string "name"
	//! return a reference to the edited state of the class

	
	

};

