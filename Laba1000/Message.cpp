#include "stdafx.h"
#include "Message.h"
#include <string>


Message::Message() : text(string ("message")) {}
Message::Message(const string &a) : text(a) {}


Message::~Message() {}

Message::Message(const Message &m) {
	text = m.text;
}

int Message::get_len()const {
	return text.size();
}

string Message::get_text() const
{
	return text;
}

void Message::set_text(const string a)
{
	text.assign(a);
}

