#include "stdafx.h"
#include "File.h"
#include "Message.h"

File::File() :typeKod(0), typeInf(0), Mail() {};

File::File(int x, int y, Mail &m) :typeKod(x), typeInf(y), Mail(m) 
{
	if ((x != 0 && x != 1) || (y != 0 && y != 1)) throw "INCORRECT TYPE OF KOD OR INFORMATION";
};

File::~File() {};

File::File(const File &a) :Mail(a) {

	typeKod = a.typeKod;
	typeInf = a.typeInf;
}



int File::get_typeKod() const {
	return typeKod;
}
int File::get_typeInf() const {
	return typeInf;
}

void File::set_typeKod(int x) {
	if (x != 0 && x != 1) throw "INCORRECT TYPE OF KOD ";
	typeKod = x;
}

void File::set_typeInf(int x) {
	if (x != 0 && x != 1) throw "INCORRECT TYPE OF  INFORMATION";
	typeInf = x;
}

File *File::convert_to_file(Mail *m)
{
	File *F = dynamic_cast<File *>(m);
	return F;
}

Mail *File::clone()
{
	return new File(*this);
}

ostream & File::print(ostream &c) const
{
	Mail::print(c);
	if (typeKod == 0)
		c << "TYPE KOD: ASCII" << endl;
	else
		c << "TYPE KOD: BIN" << endl;
	if (typeInf == 0)
		c << "TYPE INFO: CONTROL" << endl;
	else
		c << "TYPE INFO: DATA" << endl;

	c << "PACKAGE TYPE: FILE" << endl;

	return c;
}


