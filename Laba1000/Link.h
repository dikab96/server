#pragma once
#include <string>
#include <iostream>
using namespace std;


class Link
{
protected:
	int typeProt;
	string name;
public:
	Link(int, const string &);
	//! initialization constructor
	//! takes (typeProt(int),name(string))
	Link();
	//! default initialization constructor
	~Link();
	//! default destructor
	Link(const Link &);

	int get_typeProt()const;
	//! return typeProt
	string get_name()const;
	//! return name

	void set_typeProt(int );
	//! edits link's typeProt
	//! takes int
	void set_name(const string );
	//! edits link's name
	//! takes string
};

