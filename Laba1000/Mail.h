#pragma once
#include <string>
#include <iostream>
#include "Message.h"
#include <vector>

using namespace std;


class Mail{
protected:
	int from[4];
	int to[4];
	string name;
	Message mess;


public:
	friend ostream& operator <<(std::ostream &, const Mail &);
	
	Mail();
	//! default initialization constructor
	Mail(int *, int *,const string &, Message &);
	//! initialization constructor
	//! takes (array 'from'(int *),array 'to'( int *), user's name( string), message (Message))
	virtual ~Mail();
	//! virtual destructor
	Mail(const Mail &);
	Mail(Mail &&);


	int *get_to();
	//! return array 'to'
	int *get_from();
	//! return array 'from'
	string get_name()const;
	//! return user's name (string)
	Message get_message() const;
	//! return message (Message)

	void set_from(int *);
	//! edits mail's array 'from'
	//! takes int *
	void set_to(int *);
	//! edits mail's array 'to'
	//! takes int *
	void set_name(const string );
	//! edits mail user's name
	//! takes string
	void set_message(Message &);
	//! edits mail's massage
	//! takes Message

	virtual Mail *clone();
	//! make a copy of Mail
	ostream &print(ostream &) const;
	//! output function 
	//! displays full information about Mail
	//! Takes a reference to the output stream
	//! returns a reference to the output stream
};
