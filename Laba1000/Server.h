#pragma once
#include <string>
#include <iostream>
#include <queue>
#include "Mail.h"
#include "File.h"
#include "Hypertext.h"
#include "my_queue.h"
using namespace std;

class Server
{
private:
	string name;
	int adress[4];

	//queue <Mail *> Table;
	Queue <Mail *> Table;
	

public:
	Server();
	//! default initialization constructor
	~Server();
	//! default destructor
	Server(const Server &);
	Queue <Mail *> get_Table()const;
	//!
	string get_name()const;
	//! return server's name
	int *get_adress();
	//! return server's adress

	void set_name(const string );
	//! edits server's name
	//! takes string "name"
	//! return a reference to the edited state of the class
	void set_adress(int *);
	//! edits server's adress
	//! takes array of int
	//! return a reference to the edited state of the class

	Server &add_package(Mail *);
	//! add package to the table
	//! return a reference to the edited state of the class
	Mail *Find_package(int *, int);
	//! find package in the table
	//! return package
	Server &delete_package(int *, int);
	//! delete package from the table
	//! return a reference to the edited state of the class
	int determine_type(Mail &);
	//! determine type of package
	//! return number which mean type

	friend std::ostream & operator<<(std::ostream &, Server &);
	//! output function 
	//! displays full information about Server (name(string), adress(int *), table(queue <Mail *> Table))
	//! Takes a reference to the output stream
	//! returns a reference to the output stream
};

int getNum(int &);
//! input number
string getStr(string &);





/*! \mainpage My Personal Index Page
*
* \section intro_sec Introduction
*
* the program is a set of classes for working with the server
*
* describes the classes to work with three types of package:
*
* mail, file and hypertext
*
*information on all packages is stored in the Table
*
*Table is stored in the main class of the Server
*
*may add a new package to the Table
*
* \may remove the package from the Table
*
*\may get information about packages:
*
*\information about Server (name, adress)
*
*
*\upgrade Mile to File and Hypertext
*
*
*
*
* create by Kabak Dmitry
* MEPHI k03-121
*/