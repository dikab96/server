#include "stdafx.h"
#include "..\Laba1000\Message.h"
#include "..\Laba1000\Mail.h"
#include "..\Laba1000\File.h"
#include "..\Laba1000\Server.h"
#include <iostream>
#include <string>
#include <time.h>
#include <cstdlib>
#include "..\Laba1000\my_queue.h"


const char *msgs[6] = { "0. Quit", "1. Set server's name and adress", "2. Add package", "3. Find package", "4. Delete package", "5. Show table"};

int main()
{
	Message mes;
	Mail mail;
	Mail *res = nullptr;
	File file;
	Hypertext hyp;
	Server s;
	int rc = -1; int f = 0;
	string text = "a"; string name, str;
	int x, y, a, b;
	int fr[4];
	int t[4];
	Link l;
	vector <Link> link;


	while (rc != 0) {
		for (int i = 0; i < 6;i++) {
			cout << msgs[i] << endl;
		}

		getNum(rc);
		switch (rc)
		{
		case 1:
			cout << "Enter server's name: " << endl;
			getStr(name);
			s.set_name(name);
			cout << "Enter from adress: " << endl;

			for (int i = 0; i < 4; i++) {
				f = 0;
				while (f == 0) {
					try {
						f = 1;
						getNum(x);
						fr[i] = x;
					}
					catch (const char *mmm) {
						cout << mmm << endl;
						f = 0;
					}
				}

			}
			s.set_adress(fr);
			break;

		case 2:
			cout << "Enter user's name: " << endl;
			getStr(name);

			cout << "Enter message's text: " << endl;
			getStr(text);
			mes.set_text(text);

			cout << "Enter from adress: " << endl;
			for (int i = 0; i < 4; i++) {
				f = 0;
				while (f == 0) {
					try {
						f = 1;
						getNum(x);
						fr[i] = x;
					}
					catch (const char *mmm) {
						cout << mmm << endl;
						f = 0;
					}
				}

			}

			cout << "Enter to adress: " << endl;
			for (int i = 0; i < 4; i++) {
				f = 0;
				while (f == 0) {
					try {
						f = 1;
						getNum(x);
						t[i] = x;
					}
					catch (const char *mmm) {
						cout << mmm << endl;
						f = 0;
					}
				}


			}
			x = -1;
			while (x < 0 || x > 3) {
				cout << "Enter type of package (1 - Mail, 2 - File, 3 - Hypertext) : " << endl;
				getNum(x);
			}

			if (x == 1) {
				mail.set_from(fr);
				mail.set_to(t);
				mail.set_name(name);
				mail.set_message(mes);
				s.add_package(&mail);
			}

			if (x == 2) {
				file.set_from(fr);
				file.set_to(t);
				file.set_name(name);
				file.set_message(mes);
				f = 0;
				while (f == 0) {
					cout << "Enter type of kode (0 - ASCII, 1 - BIN) : " << endl;
					getNum(a);
					cout << "Enter type of information (0 - CONTROL, 1 - DATA) : " << endl;
					getNum(b);
					try {
						f = 1;
						file.set_typeKod(a);
						file.set_typeInf(b);
					}
					catch (const char*mmm) {
						cout << mmm << endl;
						f = 0;
					}
				}
				s.add_package(&file);
			}

			if (x == 3) {
				f = 0;
				while (f == 0) {
					try {
						f = 1;
						cout << "Enter type of kode (0 - ASCII, 1 - BIN) : " << endl;
						getNum(a);
						cout << "Enter type of information (0 - CONTROL, 1 - DATA) : " << endl;
						getNum(b);
					}
					catch (const char *mmm) {
						cout << mmm << endl;
						f = 0;
					}

					y = -1;
					while (y < 0) {
						cout << "Enter the number of links : " << endl;
						getNum(y);
					}
					cout << "Enter items : " << endl;
					for (int i = 0; i < y; i++) {
						f = 0;
						while (f == 0) {
							try {
								f = 1;
								cout << "Enter type of protokol : " << endl;
								getNum(a);
							}
							catch (const char *mmm) {
								cout << mmm << endl;
								f = 0;
							}
							l.set_typeProt(a);
							cout << "Enter name : " << endl;
							getStr(str);
							l.set_name(string(str));
							link.push_back(l);
						}
					}
					hyp.set_from(fr);
					hyp.set_to(t);
					hyp.set_name(name);
					hyp.set_message(mes);
					hyp.set_typeKod(a);
					hyp.set_typeInf(b);
					hyp.set_link(link);
					s.add_package(&hyp);

					break;
		case 3:
			cout << "Enter to adress: " << endl;
			for (int i = 0; i < 4; i++) {
				f = 0;
				while (f == 0) {
					try {
						f = 1;
						getNum(x);
						fr[i] = x;
					}
					catch (const char *mmm) {
						cout << mmm << endl;
						f = 0;
					}
				}

			}

			x = 5;
			while (x > 3 || x < 0) {
				cout << "Enter type of package (1 - Mail, 2 - File, 3 - Hypertext) : " << endl;
				getNum(x);
			}

			res = s.Find_package(fr, x);
			if (res == nullptr)
				cout << "There is not these package" << endl;
			else
				res->print(cout);
			break;

		case 4:
			cout << "Enter to adress: " << endl;
			for (int i = 0; i < 4; i++) {
				f = 0;
				while (f == 0) {
					try {
						f = 1;
						getNum(x);
						fr[i] = x;
					}
					catch (const char *mmm) {
						cout << mmm << endl;
						f = 0;
					}
				}

			}

			x = 4;
			while (x > 3 || x < 0) {
				cout << "Enter type of package (1 - Mail, 2 - File, 3 - Hypertext) : " << endl;
				getNum(x);
			}

			s.delete_package(fr, x);
			break;

		case 5:
			cout << (s);
			break;
				}
			}
		}
	}

	system("pause");
}


