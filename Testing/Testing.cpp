#include "stdafx.h"
#include "..\Laba1000\Message.h"
#include "..\Laba1000\Mail.h"
#include "..\Laba1000\File.h"
#include "..\Laba1000\Server.h"
#include <iostream>
#include <string>
#include "gtest\gtest.h"
#include <time.h>
#include <cstdlib>

TEST(MailTesting, CheckCONSTRUCTOR)
{
	int x, y; string str = "a";
	Message mes;
	int fr[4]; int t[4]; 
	srand(time(NULL));

	for (int j = 0; j < 10000; j++) {
		for (int i = 0; i < 4; i++) {
			x = rand() % 255;
			y = rand() % 255;
			fr[i] = x;
			t[i] = y;
		}
		Mail m(fr, t, str, mes);

		for (int n = 0; n < 4; n++) {
			EXPECT_DOUBLE_EQ(fr[n], m.get_from()[n]);
			EXPECT_DOUBLE_EQ(t[n], m.get_to()[n]);
		}
	}
};

TEST(MailTesting, CheckSETGET)
{
	int x, y; string str;
	int fr[4]; int t[4]; 
	Mail m;
	srand(time(NULL));

	for (int j = 0; j < 10000; j++) {
		for (int i = 0; i < 4; i++) {
			x = rand() % 255;
			y = rand() % 255;
			fr[i] = x;
			t[i] = y;
		}
		m.set_from(fr);
		m.set_to(t);

		for (int n = 0; n < 4; n++) {
			EXPECT_DOUBLE_EQ(fr[n], m.get_from()[n]);
			EXPECT_DOUBLE_EQ(t[n], m.get_to()[n]);
		}
	}
};


TEST(MailTesting, CheckTHROW)
{
	int x, y; string str = "a";
	Message mes;
	Mail m;
	int fr[4]; int t[4];
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 4; i++) {
			x = 260 + rand() % 500;
			y = 260 + rand() % 500;
			fr[i] = x;
			t[i] = y;
		}

		for (int n = 0; n < 4; n++) {
			EXPECT_THROW(m.set_from(fr), const char *);
			EXPECT_THROW(m.set_to(t), const char *);
		}

		for (int n = 0; n < 4; n++) {
			EXPECT_THROW(Mail n(fr, t, str, mes), const char *);
		}
	}
};

///////////////////////////////////////////////////////////////////////////////////////
TEST(FileTesting, CheckCONSTRUCTOR)
{
	int x, y;
	Mail m;
	srand(time(NULL));

	for (int j = 0; j < 10000; j++) {
			x = rand() % 1;
			y = rand() % 1;
			File f(x, y, m);
			EXPECT_DOUBLE_EQ(x, f.get_typeInf());
			EXPECT_DOUBLE_EQ(y, f.get_typeKod());
		}

};

TEST(FileTesting, CheckSETGET)
{
	int x, y;
	Mail m;
	File f;
	srand(time(NULL));

	for (int j = 0; j < 10000; j++) {
			x = rand() % 1;
			y = rand() % 1;
			f.set_typeInf(x);
			f.set_typeKod(y);
			EXPECT_DOUBLE_EQ(x, f.get_typeInf());
			EXPECT_DOUBLE_EQ(y, f.get_typeKod());
		}
};

TEST(FileTesting, CheckTHROW)
{
	int x, y;
	Mail m;
	File f;
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {
		x = 2 + rand() % 100;
		y = 2 + rand() % 100;
		EXPECT_THROW(f.set_typeInf(x), const char *);
		EXPECT_THROW(f.set_typeKod(y), const char *);
	}
};
//////////////////////////////////////////////////////////////////
TEST(HypertextTesting, CheckCONSTRUCTOR)
{
	int x, y; string str = "a";
	File f;
	Link l;
	vector <Link> link;
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {

		y = rand() % 1;
		l.set_typeProt(y);
		l.set_name(str);
		link.push_back(l);

		Hypertext h(link, f);
		EXPECT_DOUBLE_EQ(y, h.get_link()[0].get_typeProt());
	}
};

TEST(HypertextTesting, CheckSETGET)
{
	int x, y; string str = "a";
	File f;
	Link l;
	Hypertext h;
	vector <Link> link;
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {

		y = rand() % 1;
		l.set_typeProt(y);
		l.set_name(str);
		link.push_back(l);
		h.set_link(link);
		EXPECT_DOUBLE_EQ(y, h.get_link()[0].get_typeProt());
	}
};
///////////////////////////////////////////////////////////////
TEST(LinkTesting, CheckCONSTRUCTOR)
{
	int y; string str = "a";
	
	srand(time(NULL));

	for (int j = 0; j < 1000; j++) {
		y = rand() % 1;
		Link l(y, str);
		EXPECT_DOUBLE_EQ(y, l.get_typeProt());
	}
};

TEST(LinkTesting, CheckSETGET)
{
	int y; string str = "a";
	Link l;

	srand(time(NULL));

	for (int j = 0; j < 1000; j++) {
		y = rand() % 1;
		l.set_typeProt(y);
		EXPECT_DOUBLE_EQ(y, l.get_typeProt());
	}
};

TEST(LinkTesting, CheckTHROW)
{
	int y; string str = "a";
	Link l;
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {
		y = 2 + rand() % 100;
		EXPECT_THROW(l.set_typeProt(y);, const char *);
	}
};
///////////////////////////////////////////////////////////
TEST(ServerTesting, CheckSETGET)
{
	int x, y; string str = "a";
	int fr[4]; int t[4];
	queue <Mail *> Table;
	Message mes;
	Server s;
	srand(time(NULL));

	for (int j = 0; j < 10000; j++) {
		for (int i = 0; i < 4; i++) {
			x = rand() % 255;
			y = rand() % 255;
			fr[i] = x;
			t[i] = y;
		}
		s.set_adress(fr);
		
		for (int n = 0; n < 4; n++) {
			EXPECT_DOUBLE_EQ(fr[n], s.get_adress()[n]);
		}
	}
};


TEST(ServerTesting, CheckADDPACKAGE)
{
	int x, y; string str = "a";
	int fr[4]; int t[4];
	Message mes;
	Server s;
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 4; i++) {
			x = rand() % 255;
			y = rand() % 255;
			fr[i] = x;
			t[i] = y;
		}
		Mail m(fr, t, str, mes);
		s.add_package(&m);

		for (int n = 0; n < 4; n++) {
			EXPECT_DOUBLE_EQ(fr[n], s.get_Table().back()->get_from()[n]);
		}
	}
};

TEST(ServerTesting, CheckFINDPACKAGE)
{
	int x, y; string str = "a";
	int fr[4]; int t[4];
	Message mes;
	Server s;
	srand(time(NULL));

	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 4; i++) {
			//x = rand() % 255;
			//y = rand() % 255;
			fr[i] = i + j;
			t[i] = i + j;
		}
		Mail m(fr, t, str, mes);
		s.add_package(&m);
	}

	for (int i = 0; i < 100; i++) {
		for (int j = 0; j < 4; j++) {
			fr[j] = j+i;
			t[j] = j+i;
		}
	
		Mail *m = s.Find_package(t, 1);
		for (int n = 0; n < 4; n++) {
			EXPECT_DOUBLE_EQ(t[n], m->get_to()[n]);
		}
	}
	
};


int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
	system("pause");
	return 0;

}

